import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AuthGuard} from './shared/auth/auth.guard';


export function getAdminLazyModule() {
    return System.import('./admin/admin.module' + (process.env.AOT ? '.ngfactory' : ''))
        .then(mod => mod[(process.env.AOT ? 'AdminModuleNgFactory' : 'AdminModule')]);
}

export function getLoginLazyModule() {
    return System.import('./login/login.module' + (process.env.AOT ? '.ngfactory' : ''))
        .then(mod => mod[(process.env.AOT ? 'LoginModuleNgFactory' : 'LoginModule')]);
}

import {ErrorComponent} from './error/error.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {path: 'admin', loadChildren: getAdminLazyModule, canLoad: [AuthGuard]},
            {path: 'login', loadChildren: getLoginLazyModule},
            {path: '**', component: ErrorComponent}
        ])
    ],
})
export class AppRoutingModule {}
