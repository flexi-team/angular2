import {NgModule} from '@angular/core';

/* ---PAGE--- */
import {BlogModule} from './blog/blog.module';
/* --- */

import {AppRoutingModule} from './app.routing';
import {ServicesModule} from './services/services.module';
import {SharedModule} from './shared/shared.module';
import {AppComponent} from './app.component';

import {InfoService} from './shared/info-block/info-block.service';
import {APP_BASE_HREF} from '@angular/common';

@NgModule({
    declarations: [AppComponent],
    imports: [
        SharedModule,
        BlogModule,
        ErrorModule,
        AppRoutingModule, // Should be the last
    ],
    providers: [{
        provide: APP_BASE_HREF,
        useValue: '/'
    }, InfoService]
})

export class AppModule {}
export {AppComponent} from './app.component';
