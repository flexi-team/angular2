import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AuthGuard} from '../shared/auth/auth.guard';

import {AdminComponent} from './admin.component';
import {AdminService} from './admin.service';

import {UserComponent} from './dashboard/user/user.component';
import {UsersComponent} from './dashboard/user/users.component';


export const adminRoutes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [AuthGuard],
        children: [{
            path: '',
            canActivateChild: [AuthGuard],
            children: [
                {path: '', redirectTo: 'users', pathMatch: 'full'},
                {path: 'users', component: UsersComponent},
                {path: 'users/:id', component: UserComponent},
            ]
        }]
    },
];

export const MODULE_COMPONENTS = [
    AdminComponent,
    UserComponent,
    UsersComponent,
]

export const MODULE_PROVIDERS = [
    AdminService,
]