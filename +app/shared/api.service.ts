import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {contentHeaders} from './headers';
import {CacheService} from './cache.service';


import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/share';



export function hashCodeString(str: string): string {
    let hash = 0;
    if (str.length === 0) {
        return hash + '';
    }
    for (let i = 0; i < str.length; i++) {
        let char = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash;
    }
    return hash + '';
}


@Injectable()
export class ApiService {

    constructor(public _http: Http, public _cache: CacheService) {}

    get(url: string, options?: any) {
        let key = url;

        if (options && options.search) {
            key += '?' + options.search.toString();
        }

        key = hashCodeString(key);

        if (this._cache.has(key)) {
            return Observable.of(this._cache.get(key));
        }

        return this._http
            .get(url, options)
            .map(res => res.json())
            .do(json => this._cache.set(key, json))
            .share();
    }

    post(url: string, options?: any, headers?: Headers) {

        return this._http
            .post(url, (options ? options.data : null), {headers: headers || contentHeaders})
            .map(res => res.json());

    }

}