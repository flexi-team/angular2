import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ApiService} from './api.service';

//import {AUTH_PROVIDERS} from 'angular2-jwt';
import {AuthGuard} from './auth/auth.guard';
import {AuthService} from './auth/auth.service';

import {NavbarComponent} from './navbar/navbar.component';
import {InfoBlockComponent} from './info-block/info-block.component';
import {ScrollDirective} from './directive/scroll.directive';
import {ArrowTopDirective} from './directive/arrow-top.directive';
import {FocusDirective} from './directive/focus.directive';
import {InfoboxDirective} from './directive/infobox.directive';
import {ClickOutsideDirective} from './directive/click.directive';


import {TruncatePipe} from './pipes/truncate';

const MODULES = [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
];

const PIPES = [
    TruncatePipe,

];

const COMPONENTS = [
    ScrollDirective,
];

const PROVIDERS = [
    ApiService,
    AuthGuard,
    AuthService,
]

@NgModule({
    imports: [
        ...MODULES
    ],
    declarations: [
        ...PIPES,
        ...COMPONENTS
    ],
    exports: [
        ...MODULES,
        ...PIPES,
        ...COMPONENTS
    ]
})

export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                ...PROVIDERS
            ]
        };
    }
}
