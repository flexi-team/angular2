import {Component, OnInit, trigger, state, animate, transition, style} from '@angular/core';
import {Router, NavigationStart, NavigationEnd} from '@angular/router';

import {Title} from './info-block.classes';
import {InfoService} from './info-block.service';
import {isBrowser} from 'angular2-universal';

@Component({
    selector: 'info-block',
    templateUrl: './info-block.component.html',
    styleUrls: ['./style.css'],
    animations: [
        trigger('visibilityChanged', [
            state('true', style({opacity: 0})),
            transition('* => *', animate('0.3s ease')),
        ]),
        trigger('hideBlock', [
            state('true', style({height: 0, overflow: 'hidden', marginBottom: 0})),
            transition('* => *', animate('0s ease')),
        ])
    ]
})


export class InfoBlockComponent implements OnInit {

    title: Title = new Title;
    status: boolean = false;
    hideBlock: boolean = true;

    constructor(private _infoService: InfoService, private router: Router) {}

    ngOnInit() {
        this._infoService.infoBlock.subscribe(
            data => {
                if (isBrowser && this._infoService.showAnimat()) {
                    if (data.show) {
                        this.hideBlock = false;
                        this.status = true;
                        setTimeout(() => {
                            this.status = false;
                            this.title = data;
                        }, 200)
                    } else {
                        this.hideBlock = true;
                    }
                } else {
                    if (data.show) {
                        this.hideBlock = false;
                        this.title = data;
                    } else {
                        this.hideBlock = true;
                    }
                }
            }
        );
    }
}
