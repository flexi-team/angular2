import {Injectable, EventEmitter} from '@angular/core';

@Injectable()
export class InfoService {

    infoBlock = new EventEmitter();
    statusInfoBlock = new EventEmitter();

    _showAnimat = false;
    _showFooter = true;
    _scrollTop = true;

    changeInfoBlock(title: string, info: string, show: boolean = true) {
        this._showFooter = show;
        this.infoBlock.emit({label: title, text: info, show: show});
    }

    showAnimat() {
        return this._showAnimat;
    }
}