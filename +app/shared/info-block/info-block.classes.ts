export class Title {
    label: string = '';
    text: string = '';
    show: boolean = true;
}