import {Injectable} from '@angular/core';
import {style, animate, transition, state, trigger} from '@angular/core';

@Injectable()
export class Animations {
    static page = [
        trigger('routeAnimation', [
            transition(":enter", [
                style({opacity: 0}),
                animate('1s ease', style({opacity: 1}))
            ]),
            transition(":leave", [
                animate('0.2s ease', style({opacity: 0}))
            ])
        ]),
        trigger('loadPageAnimation', [
            state('*', style({
                opacity: 1
            })),
            transition('void => *', [
                style({
                    opacity: 0,
                }),
                animate('2s ease')
            ])
        ])
    ];
}

