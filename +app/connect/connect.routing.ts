import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ConnectComponent} from './connect.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {path: 'connect', component: ConnectComponent}
        ])
    ],
    exports: [RouterModule]
})
export class ConnectRoutingModule {}
