import {Component, OnInit, HostBinding} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';

import {ConnectService} from './connect.service';
import {Animations} from '../shared/animations';
import {InfoService} from '../shared/info-block/info-block.service';
import {isBrowser} from 'angular2-universal';

@Component({
    selector: 'sd-connect',
    templateUrl: './connect.component.html',
    styleUrls: ['./style.css'],
    animations: Animations.page
})

export class ConnectComponent implements OnInit {

    connectForm: FormGroup;
    showForm: boolean = true;

    @HostBinding('@routeAnimation') public routeAnimation = isBrowser;

    constructor(private _infoService: InfoService, public model: ConnectService) {

        this.connectForm = new FormGroup({
            "name": new FormControl('', [
                Validators.required
            ]),
            "city": new FormControl('', [
                Validators.required
            ]),
            "telephone": new FormControl('', [
                Validators.required,
                Validators.pattern("[0-9 +\-]{4,18}")
            ]),
            "email": new FormControl('', [
                Validators.required,
                Validators.pattern("[a-zA-Z_0-9]+@[a-zA-Z_]+\.[a-zA-Z]{2,3}")
            ])
        });
    }

    close() {
        this.showForm = false;
        this.connectForm.reset();
    }

    submit() {

        if (this.connectForm.controls['name'].valid && (this.connectForm.controls['telephone'].valid || this.connectForm.controls['email'].valid)) {
            this.model.connectForm(this.connectForm.value).subscribe(
                data => {
                    this.close();
                },
                error => {
                    console.log(error);
                    this.close();
                }
            );
        } else {
            let formErrors = this.connectForm.controls;
            for (const field in formErrors) {
                this.connectForm.get(field).markAsDirty();
            }
        }
    }

    ngOnInit() {
        this._showInfoBlock();
    }

    private _showInfoBlock() {
        this._infoService.changeInfoBlock('', '', false);
    }
}