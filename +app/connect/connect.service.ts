import {Injectable} from '@angular/core';
import {URLSearchParams, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {GlobalVariable} from '../global';

import {ApiService} from '../shared/api.service';

@Injectable() export class ConnectService {

    constructor(public api: ApiService) {}

    connectForm(obj: any) {
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let params = new URLSearchParams();

        for (let key in obj) {
            params.set(key, obj[key]);
        }

        return this.api.post(GlobalVariable.NODE_API_URL + '/connect', {data: params.toString()}, headers);
    }

}