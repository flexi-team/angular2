import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConnectComponent} from './connect.component';
import {ConnectRoutingModule} from './connect.routing';
import {SharedModule} from '../shared/shared.module';
import {ConnectService} from './connect.service';

import {Animations} from '../shared/animations';

@NgModule({
    imports: [CommonModule, ConnectRoutingModule, SharedModule],
    declarations: [ConnectComponent],
    exports: [ConnectComponent],
    providers: [Animations, ConnectService]
})
export class ConnectModule {}
