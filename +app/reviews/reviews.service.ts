import {Injectable} from '@angular/core';
import {URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {GlobalVariable} from '../global';

import {ApiService} from '../shared/api.service';

@Injectable() export class ReviewsService {

    constructor(public api: ApiService) {}

    getData(page: number = 0, rand?: boolean): Observable<string[]> {

        let params: URLSearchParams = new URLSearchParams();
        if (page) {
            params.set('page', <string> <any> page);
        }
        if (rand) {
            params.set('rand', 'true');
        }

        return this.api.get(GlobalVariable.NODE_API_URL + '/reviews', {
            search: params
        });
    }
}