import {Component, OnInit, ViewChild, HostBinding} from '@angular/core';
import {ScrollDirective} from '../shared/directive/scroll.directive';
import {Animations} from '../shared/animations';
import {InfoService} from '../shared/info-block/info-block.service';
import {ReviewsService} from './reviews.service';
import {isBrowser} from 'angular2-universal';

let host = {};

if (isBrowser) {
    host = {'scroll': 'true'};
}

@Component({
    selector: 'sd-reviews',
    templateUrl: './reviews.component.html',
    host: host,
    styleUrls: ['../styles/testimonials.css'],
    animations: Animations.page
})

export class ReviewsComponent implements OnInit {

    @HostBinding('@routeAnimation') public routeAnimation = isBrowser;

    constructor(public model: ReviewsService, private _infoService: InfoService) {}

    ngOnInit() {
        this._showInfoBlock();
    }

    private _showInfoBlock() {

        let lable = 'Reviews';
        let text = `We've had an amazing experience to work with big projects and small start-ups from different countries creating products on various topics.`
        this._infoService.changeInfoBlock(lable, text);
    }


    @ViewChild(ScrollDirective) scroll: ScrollDirective;

    reviews: Array<any> = [];

    loadingMoreItems: boolean = true;
    noMoreItems: boolean = false;
    at: number = 800;
    stairs: boolean = true;

    full: boolean = false;
    openFull: () => void;

    fullPost: any;

    closeFull() {
        this.full = false;
        this.fullPost.full = false;
        setTimeout(() => {
            this.scroll.buildBlocks();
        }, 300);
    }

    getItems(pageNumber: number): void {
        this.loadingMoreItems = false;
        this.model.getData(pageNumber).subscribe(
            data => {
                let _data = data['resp'];
                this.loadingMoreItems = true;
                this.noMoreItems = (_data.length === 0);

                let self = this;

                _data.map(function (post: any) {
                    post.flagUrl = 'build/img/flags/4x3/' + post.country.toLowerCase() + '.svg';
                    post.openFull = function () {
                        self.fullPost = post;
                        post.full = true;
                        self.full = true;
                    }
                });

                this.reviews = pageNumber > 1 ? this.reviews.concat(_data) : _data;
            }
        );
    }
}