import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReviewsComponent} from './reviews.component';


import {ReviewsRoutingModule} from './reviews.routing';
import {SharedModule} from '../shared/shared.module';
import {ReviewsService} from './reviews.service';

import {Animations} from '../shared/animations';

@NgModule({
    imports: [CommonModule, ReviewsRoutingModule, SharedModule],
    declarations: [ReviewsComponent],
    exports: [ReviewsComponent],
    providers: [ReviewsService, Animations]
})
export class ReviewsModule {}
