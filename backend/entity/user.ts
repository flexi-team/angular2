import {Entity, PrimaryColumn, Column, ManyToMany, JoinTable, OneToMany} from "typeorm";
import {Project} from "./project";

@Entity()
export class User {

    @PrimaryColumn("int", {
        generated: true
    })
    id: number;

    @Column()
    username: string;

    @Column()
    position: string;

    @Column()
    avatar: string;

    @OneToMany(type => Project, post => post.developer)
    project: Project[];

    @Column()
    date: number;

    @Column()
    date_change: number;

}
