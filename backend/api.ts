import {Router, Request, Response, NextFunction} from 'express';
import {Db} from './config';

import {Login} from './login';

import {User} from './entity/user';
import {Project} from './entity/project';


const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();
const passport = require('passport');


import {createConnection, ConnectionOptions} from "typeorm";
const connectionOptions: ConnectionOptions = {
    driver: {
        type: 'mysql',
        host: "localhost",
        port: Db.db_port,
        username: Db.db_user,
        password: Db.db_password,
        database: Db.db_name
    },
    entities: [User],
    autoSchemaSync: true
};

createConnection(connectionOptions);

export function apiRouter() {

    const router = Router()

    /*-------Admin------*/
    /*-----Images for editor-----*/
    router.route('/load-images').get(Login.isAuth, Images.load_images);
    router.route('/delete_image').post(Login.isAuth, Images.delete_image);
    router.route('/upload_image').post(Login.isAuth, Images.upload_image);
    /*-----End-----*/

    /*-------End Admin------*/


    /*---- Frontend ----*/
    router.route('/login').post(Login.post);
    router.route('/posts').get(Blog.getWithLimit);

    /*--------End-----*/

    return router;
};
