var jwt = require('jsonwebtoken');
var _ = require('lodash');

var users = [{
    id: 1,
    username: '***',
    password: '***'
}];

import {Router, Request, Response, NextFunction} from 'express';

export const Login = {

    isAuth(req: Request, res: Response, next: NextFunction): void {

        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (token) {

            jwt.verify(token, '*****************', function (err, decoded) {
                if (err) {
                    return res.status(403).send({
                        message: 'Failed to authenticate token.'
                    });
                } else {
                    next();
                }
            });

        } else {
            res.status(403).send({
                message: 'No token provided.'
            });
        }
    },
    createToken: (user): string => {
        return jwt.sign(_.omit(user, 'password'), '*******************', {
            expiresIn: 60 * 60 * 24
        });
    },
    post(req: Request, res: Response, next: NextFunction): void {

        if (!req.body.username || !req.body.password) {
            res.status(400).send("You must send the username and the password");
        }

        var user = _.find(users, {username: req.body.username});

        if (!user) {
            res.status(401).send("The username or password don't match");
        }

        if (!(user.password === req.body.password)) {
            res.status(401).send("The username or password don't match");
        }

        res.status(201).send({
            id_token: Login.createToken(user)
        });
    },

};
