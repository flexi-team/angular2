import {Router, Request, Response, NextFunction} from 'express';

import {Post} from "./entity/post";
import {getConnection} from "typeorm";
import {Images} from './image';

export const Blog = {

    _getModel: () => {
        return getConnection().getRepository(Post);
    },

    async getWithLimit(req: Request, res: Response, next: NextFunction) {

        let page = req.query['page'] ? +req.query['page'] - 1 : 0;
        let limit = 10;

        let [posts, count] = await Blog._getModel().createQueryBuilder("blog")
            .setLimit(limit)
            .setOffset(limit * page)
            .getManyAndCount();

        res.status(200).send({
            status: {
                code: 200,
                message: ''
            },
            resp: posts,
            count: count
        });
    },

    async getUrlPost(req: Request, res: Response, next: NextFunction) {

        let posts = await Blog._getModel().createQueryBuilder("blog").where('blog.url=:url', {url: req.params.url}).getOne();

        res.status(200).send({
            status: {
                code: 200,
                message: ''
            },
            resp: posts
        });
    },

    async get(req: Request, res: Response, next: NextFunction) {

        let posts = await Blog._getModel().createQueryBuilder("blog")
            .orderBy('blog.date', 'DESC')
            .getMany();

        res.status(200).send({
            status: {
                code: 200,
                message: ''
            },
            resp: posts
        });
    },

    async post(req: Request, res: Response, next: NextFunction) {

        let _POST = req.body;

        let newPost = new Post();
        if (_POST.id) {
            var currentPost = await Blog._getModel().findOneById(_POST.id);
            if (currentPost) {
                newPost = currentPost;
                if (!_POST.preview && currentPost.preview) {
                    Images.removeDir('blog/' + currentPost.id);
                }
            }
        } else {
            newPost.url = Blog.urlRusLat(_POST.title);

            let isUrl = await Blog._getModel().createQueryBuilder("blog")
                .where('blog.url =:url', {url: newPost.url})
                .getMany();

            if (isUrl.length) {
                newPost.url += '-' + isUrl.length;
            }

        }

        newPost.title = _POST.title;
        newPost.text = _POST.text;
        newPost.date = _POST.date;
        newPost.date_change = new Date().getTime();

        let saveObj = await Blog._getModel().persist(newPost);

        if (req['files'] && req['files']['files']) {
            await Images.saveImage(req['files']['files'], 'blog/' + saveObj.id, req).then(
                result => {
                    saveObj.preview = result['preview'];
                    Blog._getModel().persist(saveObj);
                },
                reject => {
                    console.log(reject);
                }
            );
        }

        res.status(200).send({
            status: {
                code: 200,
                message: 'Post successfully saved.'
            },
            resp: {
                id: saveObj.id
            }
        });
    },

    urlRusLat(str: string): string {

        str = str.toLowerCase();
        const cyr2latChars = new Array(
            ['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'],
            ['д', 'd'], ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'],
            ['и', 'i'], ['й', 'y'], ['к', 'k'], ['л', 'l'],
            ['м', 'm'], ['н', 'n'], ['о', 'o'], ['п', 'p'], ['р', 'r'],
            ['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'],
            ['х', 'h'], ['ц', 'c'], ['ч', 'ch'], ['ш', 'sh'], ['щ', 'shch'],
            ['ъ', ''], ['ы', 'y'], ['ь', ''], ['э', 'e'], ['ю', 'yu'], ['я', 'ya'],

            ['А', 'A'], ['Б', 'B'], ['В', 'V'], ['Г', 'G'],
            ['Д', 'D'], ['Е', 'E'], ['Ё', 'YO'], ['Ж', 'ZH'], ['З', 'Z'],
            ['И', 'I'], ['Й', 'Y'], ['К', 'K'], ['Л', 'L'],
            ['М', 'M'], ['Н', 'N'], ['О', 'O'], ['П', 'P'], ['Р', 'R'],
            ['С', 'S'], ['Т', 'T'], ['У', 'U'], ['Ф', 'F'],
            ['Х', 'H'], ['Ц', 'C'], ['Ч', 'CH'], ['Ш', 'SH'], ['Щ', 'SHCH'],
            ['Ъ', ''], ['Ы', 'Y'],
            ['Ь', ''],
            ['Э', 'E'],
            ['Ю', 'YU'],
            ['Я', 'YA'],

            ['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'],
            ['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'],
            ['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'],
            ['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'],
            ['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'],
            ['z', 'z'],

            ['A', 'A'], ['B', 'B'], ['C', 'C'], ['D', 'D'], ['E', 'E'],
            ['F', 'F'], ['G', 'G'], ['H', 'H'], ['I', 'I'], ['J', 'J'], ['K', 'K'],
            ['L', 'L'], ['M', 'M'], ['N', 'N'], ['O', 'O'], ['P', 'P'],
            ['Q', 'Q'], ['R', 'R'], ['S', 'S'], ['T', 'T'], ['U', 'U'], ['V', 'V'],
            ['W', 'W'], ['X', 'X'], ['Y', 'Y'], ['Z', 'Z'],

            [' ', '-'], ['0', '0'], ['1', '1'], ['2', '2'], ['3', '3'],
            ['4', '4'], ['5', '5'], ['6', '6'], ['7', '7'], ['8', '8'], ['9', '9'],
            ['-', '-']

        );

        let newStr = new String();

        for (let i = 0; i < str.length; i++) {

            let ch = str.charAt(i);
            let newCh = '';

            for (let j = 0; j < cyr2latChars.length; j++) {
                if (ch == cyr2latChars[j][0]) {
                    newCh = cyr2latChars[j][1];
                }
            }
            newStr += newCh;
        }

        return newStr.replace(/[_]{2,}/gim, '_').replace(/\n/gim, '');
    }

};

